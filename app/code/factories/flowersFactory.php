<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 3:42 AM
 */

interface FlowersFactoryInterface{
    public  function getFlower($flowerName = null);
}