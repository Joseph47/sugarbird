<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 11:06 PM
 */


spl_autoload_register(function () {

    require_once "app/code/observers/Subject.php";
    require_once "app/code/observers/Observer.php";

    require_once "app/code/Model/logistics/TimeOfDay.php";
    require_once "app/code/Model/logistics/Sun.php";

    include "app/code/Model/flowers/Flower.php" ;
    include "app/code/Model/animal/Animal.php";

    include "app/code/Model/flowers/SunFlower.php" ;
    include "app/code/Model/animal/Sugarbird.php";

});


