<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 3:35 AM
 */

abstract class Flower implements Observer {

    //todo: flowers remember everthing

    private $openStatus = false;
    private $fedOnCount = 0;
    private $animalCopacity = null; //TODO maybe constant ?


    public function __construct(){
    }

    private function getFedOnCount(){
        return $this->fedOnCount;
    }

    public function canBeFedOn(){
        return ($this->getFedOnCount() < constant('maxNectar') && ($this->getOpenStatus()));
    }

    //todo change to water
    public function feed(){
        if($this->canBeFedOn()){
            $this->fedOnCount++;
        }
    }

    public function setOpenStatus($status = false){
        $this->openStatus = $status;
    }
    public function getOpenStatus(){
        return $this->openStatus;
    }

}