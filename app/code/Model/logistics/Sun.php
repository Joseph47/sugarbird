<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 5:08 AM
 */

final class Sun extends TimeOfDay implements Subject {
    const MAX_OUT_TIME = 12;

    private $isSunOut = null;
    private static $sun = null;
    private $eventCollection = array();

    private function __construct(){
        $this->countsCompleted = 0;
        $this->isDay = true;
        $GLOBALS["messageDisplay"]("Early bird get that wirm");
        $GLOBALS["messageDisplay"]("hours left :".$this->getMaxCountAllowed() );
    }

    public static function getSingletonSun()
    {
        if( self::$sun === null){
            self::$sun = new Sun();
        }
       return  self::$sun;
    }

    public function rise(){
        if($this->isSunOut === true){
            echo "sun is already out, must be cloudy";
        }else{
            $this->isSunOut = true;

            $GLOBALS["messageDisplay"]("the sun rose.");

            echo "current hour left : ". $this->getCountsLeft().PHP_EOL;
            $this->addHourPassed(1);

            $this->notifyObservers();
        }
    }

    public function addHourPassed( $count = null){
        //todo should be in timeofday class
        if($this->isCountIncrementAllowed($count) ){
            return $this->countsCompleted += $count;
            echo "HOUR CHANGE (". $this->countsCompleted . ")";

            $this->notifyObserver();
        }
    }

     public function register($eventName = null, $observer = null)
     {
         if(isset($eventName) && isset($observer)){
             $hashId  = spl_object_hash($observer);
             $this->eventCollection[$hashId] = $observer;
         }
     }

       public function unRegister($observer = null)
       {
           $hashId  = spl_object_hash($observer);
           unset($this->eventCollection[$hashId]);
       }

       public function notifyObservers()
       {
           foreach ($this->eventCollection as $key => $value){
               $value->update();
           }
       }
}
