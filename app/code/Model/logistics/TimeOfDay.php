<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 3:49 AM
 */

define("MAX_COUNT_CYCLES",12);
define("ONE_HOUR",1);

//TODO: make this a singleton
abstract class TimeOfDay {

    protected $isDay = null;
    protected $countsCompleted = null;

    private function __construct(){}

    private function resetCountsCompleted(){
        $this->countsCompleted = null;
    }

    protected function changeIsDayStatus(){
        if($this->isDay){
            $this->isDay = false;
            $this->resetCountsCompleted();
            //TODO dispatch day ends
            echo "DAY END (". $this->countsCompleted . ")";

        }else{
            $this->isDay = true;
            $this->resetCountsCompleted();
            //TODO dispatch day start
            echo "DAY START (". $this->countsCompleted . ")";
        }
    }

    protected function isCountIncrementAllowed(int $count = null){
        return (($this->getCountsCompleted() + $count) <  $this->getMaxCountAllowed()) ? true : false;
    }


    public static function  getSingletonTimeOfDay(){
        static $timeOfDay;

        if( ! isset($timeOfDay)){
            $timeOfDay = new TimeOfDayFactory();
        }
        return $timeOfDay;
    }

    protected function getMaxCountAllowed(){
        return constant("MAX_COUNT_CYCLES");
    }

    public function getCountsLeft(){
        return $this->getMaxCountAllowed() - $this->getCountsCompleted();
    }

    public function getCountsCompleted(){
        return $this->countsCompleted;
    }

    public function isDay(){
        return $this->isDay;
    }

    public function addCount( $count = null){
        if($this->isCountIncrementAllowed($count) ){
           return $this->countsCompleted += $count;
           echo "HOUR CHANGE (". $this->countsCompleted . ")";
           //TODO dispatch count added
        }
    }
}