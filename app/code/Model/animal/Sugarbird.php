<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 4:19 PM
 */

define("MIN_RANDOM_NUMBER",1);
define("MAX_RANDOM_NUMber",10);

//todo makes sense for the bird to remember where it has been
class Sugarbird extends \Animal
{
    private $birdId = null;
    private $visitedFeedingGround = Array(); //todo get better name
    protected $observers = array();
    private $isAwake = null;

    public function __construct()
    {
        echo "created bird".PHP_EOL;
        $this->isAwake = false;
    }

    private function selectNewFeedingLocation(){
        return mt_rand(MIN_RANDOM_NUMBER,MAX_RANDOM_NUMber);
    }

    public function feed($foodProspect = null){
        //todo feed on current location.
        if($this->move()){
            //check if plant can be fed on, plant should not allow
            echo "feeding on :" .$this->visitedFeedingGround[$foodProspect];
        }
    }

    public function move(){
        //todo
        $foundFlower = false;
        $foodProspect = $this->selectNewFeedingLocation();
        if( ! in_array($foodProspect,$this->visitedFeedingGround)){
            array_push($visitedFeedingGround, $foodProspect);
            echo "Moved to location ".$foodProspect;
            $foundFlower = true;
        }else{
            echo "already fed at location : ". $foodProspect;
            if($this->visitedFeedingGround < \FlowerBed::MAX_FLOWERS_HELD){
                $this->move();
            }
            //todo refactor : stop loop for when array full. poor birdy
        }
        return $foundFlower;
    }
    public function isAwake()
    {
        return $this->isAwake;
    }
    public function wakeUp()
    {
        $this->isAwake = true;
    }

    public function update()
    {
        // TODO: Implement update() method.
        echo "sugarbird update".PHP_EOL;
        if($this->isAwake()){
            $this->feed($this->selectNewFeedingLocation());
        }else{
            $this->wakeUp();
        }
    }

   }