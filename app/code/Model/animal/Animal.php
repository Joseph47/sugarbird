<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 4:17 PM
 */

abstract class Animal implements Observer
{
    public function feed(){}
    public function move(){}
    public function wakeUp(){}
    public function isAwake(){}
}