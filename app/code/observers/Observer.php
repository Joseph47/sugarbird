<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 12:23 PM
 */

interface Observer {
    public function update();
}