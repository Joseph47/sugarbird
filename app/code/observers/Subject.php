<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 12:22 PM
 */

interface Subject {
    public function  register($eventName = null,$observer = null);
    public function  unRegister($observer = null);
    public function  notifyObservers();
}