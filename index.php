<?php
/**
 * Created by PhpStorm.
 * User: j
 * Date: 2018/06/04
 * Time: 3:34 AM
 */

require_once "app/code/etc/includes.php";
$messageDisplay = function ($message = null){
    echo $message . PHP_EOL;
};

try{


    $sunObjSingleton =  Sun::getSingletonSun();

    $birdObj = new Sugarbird();
    $flowerObj = new SunFlower();

    $sunObjSingleton->register("bird",$birdObj);
    $sunObjSingleton->register("flower",$flowerObj);
    $sunObjSingleton->rise();

echo "is day :".$sunObjSingleton->isDay();

    while ($sunObjSingleton->isDay()){
        $sunObjSingleton->addHourPassed($sunObjSingleton->getCountsCompleted());
    }

}catch (\Exception $e){
    echo $e;
}